# Créer une version de Départements & Notaires

* Vérifier que les commit envoyés ont bien été validés par la CI de Gitlab\
Présence d'une coche verte ![Commit validé](../Images/pipeline_ok.png) associée à un commit 
* Modifier le numéro de version dans `appli_sf/composer.json`, champ `version`
* Mettre à jour le CHANGELOG
* Tagguer le commit avec le même numéro de version que dans le composer.json (le build associé au tag comportera l'archive nommée avec le bon numéro de version) : git tag -a 2.1.2 -m "2.1.2"
* Pousser le commit et le tag sur le GitLab