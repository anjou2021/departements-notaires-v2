# Données CSV de test

## Pré-requis

Avoir [importé les données CSV](Import-individus.md) (de test) des individus

## Utilisation du jeu de test

Le jeu de test importé permet de tester tous les cas de résultats de recherche.
Voici la liste des critères de recherche à renseigner pour retrouver le cas souhaité :

### Cas 1 Résultat attendu : connue, aide récupérable

- Date de décès : 10/02/2020
- Lieu de décès : VILLEFRANCHE
- Date de l'acte de décès : 12/02/2020
- Prénom : Aimé
- Nom d'usage : OLMO
- Date de naissance : 06/12/1977

### Cas 2 : Résultat attendu : connue, aide non récupérable

- Date de décès : 10/02/2020
- Lieu de décès : VILLEFRANCHE
- Date de l'acte de décès : 12/02/2020
- Prénom : Maesson
- Nom d'usage : ALOUANE
- Date de naissance : 03/12/1954

### Cas 3 : Résultat attendu : inconnue

- Date de décès : 10/02/2020
- Lieu de décès : VILLEFRANCHE
- Date de l'acte de décès : 12/02/2020
- Prénom : Mickael
- Nom d'usage : Dupont
- Date de naissance : 07/08/1986

### Cas 4 : Résultat attendu : inconnue, demande en cours d'instruction
 
- Date de décès : 10/02/2020
- Lieu de décès : VILLEFRANCHE
- Date de l'acte de décès : 12/02/2020
- Prénom : Yvette
- Nom d'usage : ROSA
- Date de naissance : 08/12/1968

### Cas 5 : Résultat attendu : ambigu

- Date de décès : 10/02/2020
- Lieu de décès : VILLEFRANCHE
- Date de l'acte de décès : 12/02/2020
- Prénom : Clara
- Nom d'usage : BOURDIAU
- Date de naissance : 01/09/1966
