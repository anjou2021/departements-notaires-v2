# Personnalisation de l'application

Sont décrites ici les étapes pour personnaliser l'application.

Cette personnalisation vous permettra de modifier tous les "templates" de l'application (y compris les emails et les pdfs).

# Créer un nouveau template

A la racine de l'application, dans le répertoire `templates`, créer un nouveau dossier, par exemple `montemplate`

Copier à l'intérieur l'intégralité du dossier `templates/default` (ou uniquement le ou les fichiers templates que vous souhaitez modifier)

Vous êtes maintenant libre de modifier complétement les templates du dossier `templates/montemplate`

# Associer le template créé à l'application

Il ne reste plus qu'a configurer l'application pour lui dire d'utiliser le nouveau template créé.

Pour cela il faut modifier le fichier `.env` à la racine de l'application.

Il suffit de remplacer la valeur de la variable `APP_THEME`, par exemple `APP_THEME=montemplate`

Si aucune valeur n'est renseigné pour `APP_THEME`, l'application utilise par défaut les templates du dossier `templates/default`

# Arborescence du dossier template

* `account` : templates des informations utiliateurs (pages "mon compte" et "réinitialisation du mot de passe")
* `admin` : templates des pages l'administration (instructeurs, utilisateurs, logs et paramètres)
* `emails` : templates des emails
* `form` et `macro` : templates servant à l'affichage des champs des formulaires
* `pdf` : templates des pdf
* `search` : templates des pages recherche et historique des recherches
* `security` : templates liés au système de connexion (mot de passe, connexion, ....)
* `stats` : templates des pages statistiques
* `utils` : templates des entetes et pieds de pages

# Ajout de classes CSS personnalisées pour modifier l'apparence graphique de l'application

Il est possible de modifier ou de surcharger les classes CSS utilisées par l'application.

Pour cela il suffit d'intégrer le code CSS souhaité dans le fichier à la racine de l'application `public/styles.css`
