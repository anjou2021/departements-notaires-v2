document.addEventListener("DOMContentLoaded", function () {
    let switchButtons = document.querySelectorAll('.switch-form .switch');

    switchButtons.forEach(function (switchButton) {
        switchButton.addEventListener('click', function () {
            let form = this.closest('form');

            form.submit();
        })
    });
});
