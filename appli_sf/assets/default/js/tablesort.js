const tablesort = require('tablesort');

module.exports = function() {
    let elements = document.getElementsByClassName('table-sort');

    for (let i = 0; i < elements.length; i++) {
        tablesort(elements[i]);
    }
};
