var moment = require('moment');

document.addEventListener("DOMContentLoaded", function () {
    let deathDateInput = document.getElementById('search_deathDate');
    let deathCertificateDateInput = document.getElementById('search_deathCertificateDate');
    let birthDateInput = document.getElementById('search_birthDate');
    let dateList = [];
    let messageDiv = document.getElementById('js_error');

    deathDateInput.addEventListener('blur', (event) => {
        deathDateValidation(deathDateInput, dateList, messageDiv);
    });

    deathCertificateDateInput.addEventListener('blur', (event) => {
        deathCertificateDateValidation(deathCertificateDateInput, dateList, messageDiv);
    });

    birthDateInput.addEventListener('blur', (event) => {
        birthDateValidation(birthDateInput, dateList, messageDiv);
    });
});

function deathDateValidation(elem, dateList, messageDiv) {
    let date = moment(elem.value, 'DD/MM/YYYY');
    dateList['deathDate'] = date;

    if (date.isAfter(moment())) {
        showMessage(messageDiv, translations.deathDateAfterNow);
        elem.value = '';
    } else if (dateList['deathCertificateDate'] && date.isAfter(dateList['deathCertificateDate'])) {
        showMessage(messageDiv, translations.deathDateAfterdeathCertificateDate);
        elem.value = '';
    } else {
        hideMessage(messageDiv);
    }
}

function deathCertificateDateValidation(elem, dateList, messageDiv) {
    let date = moment(elem.value, 'DD/MM/YYYY');
    dateList['deathCertificateDate'] = date;

    if (date.isAfter(moment())) {
        showMessage(messageDiv, translations.deathCertificateDateAfterNow);
        elem.value = '';
    } else if (date.isBefore(dateList['deathDate'])) {
        showMessage(messageDiv, translations.deathDateAfterdeathCertificateDate);
        elem.value = '';
    } else {
        hideMessage(messageDiv);
    }
}

function birthDateValidation(elem, dateList, messageDiv) {
    let date = moment(elem.value, 'DD/MM/YYYY');
    dateList['birthDate'] = date;

    if (date.isAfter(moment())) {
        showMessage(messageDiv, translations.birthDateAfterNow);
        elem.value = '';
    } else {
        hideMessage(messageDiv);
    }
}

function showMessage(messageDiv, text) {
    messageDiv.textContent = text;
    messageDiv.style.display = 'block';
}

function hideMessage(messageDiv) {
    messageDiv.textContent = '';
    messageDiv.style.display = 'none';
}
