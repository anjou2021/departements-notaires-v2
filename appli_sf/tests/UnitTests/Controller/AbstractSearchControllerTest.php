<?php

namespace App\Tests\UnitTests\Controller;

use App\Controller\SearchController;
use App\Data\SearchData;
use App\Data\SettingsData;
use App\Entity\Person;
use App\Helper\SearchHelper;
use App\Service\SettingsDataService;
use App\Service\SettingService;
use App\Tests\UnitTests\TestCase\FixtureAwareTestCase;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractSearchControllerTest extends FixtureAwareTestCase
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var SettingService
     */
    protected $settingService;

    /**
     * @var SettingsDataService
     */
    protected $settingsDataService;

    /**
     * @var SettingsData
     */
    protected $settingsData;

    /**
     * Test recherche d'une personne connue, aide récupérable
     */
    public function testFindRecPerson()
    {
        $this->searchFindRecPerson(false);
    }

    /**
     * Test recherche d'une personne connue, aide non récupérable
     */
    public function testFindNotRecPerson()
    {
        $this->searchFindNotRecPerson(false);
    }

    /**
     * Test recherche d'une personne inconnue
     */
    public function testNotFindPerson()
    {
        $this->searchNotFindPerson(false);
    }

    /**
     * Test recherche d'une personne avec résultat ambigu
     */
    public function testAmbiguousPerson()
    {
        $this->searchAmbiguousPerson(false);
    }

    /**
     * Test recherche d'une personne inconnue (sans aide) avec demande en cours
     */
    public function testNotFindRequestInProgressPerson()
    {
        // Sans SOUNDEX
        $this->searchNotFindRequestInProgressPerson(false);

        // Avec SOUNDEX
        $this->searchNotFindRequestInProgressPerson(true);
    }

    /**
     * [SOUNDEX] Test recherche d'une personne connue, aide récupérable
     */
    public function testFindRecPersonWithSoundex()
    {
        $this->searchFindRecPerson(true);
    }

    /**
     * [SOUNDEX] Test recherche d'une personne connue, aide non récupérable
     */
    public function testFindNotRecPersonWithSoundex()
    {
        $this->searchFindNotRecPerson(true);
    }

    /**
     * [SOUNDEX] Test recherche d'une personne inconnue
     */
    public function testNotFindPersonWithSoundex()
    {
        $this->searchNotFindPerson(true);
    }

    /**
     * [SOUNDEX] Test recherche d'une personne avec résultat ambigu
     */
    public function testAmbiguousPersonWithSoundex()
    {
        $this->searchAmbiguousPerson(true);
    }

    /**
     * [SOUNDEX] Test recherche d'une personne inconnue (sans aide) avec demande en cours
     */
    public function testNotFindRequestInProgressPersonWithSoundex()
    {
        $this->searchNotFindRequestInProgressPerson(true);
    }

    /**
     * Test alerte décès
     */
    public function testDeathAlert()
    {
        // Test qui doit déclencher une alerte décès
        $this->searchDeathAlertPerson(true);

        // Test qui ne doit pas déclencher une alerte décès
        $this->searchDeathAlertPerson(false);
    }

    abstract protected function initSettingsDataService();

    /**
     * @param bool $useSoundex
     *
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \ReflectionException
     */
    private function searchFindRecPerson(bool $useSoundex)
    {
        $this->initSettingsDataService();
        $searchData = new SearchData();

        $searchData->setDeathDate(\DateTime::createFromFormat('j/m/Y', '10/02/2020'));
        $searchData->setDeathLocation('VILLEFRANCHE');
        $searchData->setDeathCertificateDate(\DateTime::createFromFormat('j/m/Y', '10/02/2020'));
        $searchData->setFirstName('Aimé');

        switch ($this->settingsDataService->getData()->getSearchByName()) {
            case SearchHelper::SEARCH_BY_CIVIL_NAME:
                $searchData->setCivilName('OLMO');
                break;
            default:
                $searchData->setUseName('OLMO');
        }

        $searchData->setBirthDate(\DateTime::createFromFormat('j/m/Y', '06/12/1977'));

        $personRepository = $this->entityManager->getRepository(Person::class);

        $controller = new SearchController($this->settingsDataService, $this->translator, 'default');

        /**
         * @var string $searchStatus
         * @var Person $person
         */
        [$searchStatus, $person] = $this->invokeMethod(
            $controller,
            'searchPerson',
            [$personRepository, $searchData, $useSoundex]
        );

        $nameField = $this->settingsDataService->getData()->getSearchByName() === SearchHelper::SEARCH_BY_USE_NAME ?
            'useName' : 'civilName';

        $expectedPerson = $personRepository->findOneBy(
            [
                'firstName' => 'Aime',
                $nameField  => 'OLMO',
                'birthDate' => \DateTime::createFromFormat('j/m/Y', '06/12/1977'),
            ]
        );

        $this->assertSame(SearchHelper::SEARCH_STATUS_FIND_REC, $searchStatus);
        $this->assertSame($expectedPerson, $person);
    }

    /**
     * @param bool $useSoundex
     *
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \ReflectionException
     */
    private function searchFindNotRecPerson(bool $useSoundex)
    {
        $this->initSettingsDataService();
        $searchData = new SearchData();

        $searchData->setDeathDate(\DateTime::createFromFormat('j/m/Y', '10/02/2020'));
        $searchData->setDeathLocation('VILLEFRANCHE');
        $searchData->setDeathCertificateDate(\DateTime::createFromFormat('j/m/Y', '10/02/2020'));
        $searchData->setFirstName('Maesson');

        switch ($this->settingsDataService->getData()->getSearchByName()) {
            case SearchHelper::SEARCH_BY_CIVIL_NAME:
                $searchData->setCivilName('ALOUANE');
                break;
            default:
                $searchData->setUseName('ALOUANE');
        }

        $searchData->setBirthDate(\DateTime::createFromFormat('j/m/Y', '03/12/1954'));

        $personRepository = $this->entityManager->getRepository(Person::class);

        $controller = new SearchController($this->settingsDataService, $this->translator, 'default');

        /**
         * @var string $searchStatus
         * @var Person $person
         */
        [$searchStatus, $person] = $this->invokeMethod(
            $controller,
            'searchPerson',
            [$personRepository, $searchData, $useSoundex]
        );

        $nameField = $this->settingsDataService->getData()->getSearchByName() === SearchHelper::SEARCH_BY_USE_NAME ?
            'useName' : 'civilName';

        $expectedPerson = $personRepository->findOneBy(
            [
                'firstName' => 'Maesson',
                $nameField  => 'ALOUANE',
                'birthDate' => \DateTime::createFromFormat('j/m/Y', '03/12/1954'),
            ]
        );

        $this->assertSame(SearchHelper::SEARCH_STATUS_FIND_NOTREC, $searchStatus);
        $this->assertSame($expectedPerson, $person);
    }

    /**
     * @param bool $useSoundex
     *
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \ReflectionException
     */
    private function searchNotFindPerson(bool $useSoundex)
    {
        $this->initSettingsDataService();
        $searchData = new SearchData();

        $searchData->setDeathDate(\DateTime::createFromFormat('j/m/Y', '10/02/2020'));
        $searchData->setDeathLocation('VILLEFRANCHE');
        $searchData->setDeathCertificateDate(\DateTime::createFromFormat('j/m/Y', '10/02/2020'));
        $searchData->setFirstName('Mickael');

        switch ($this->settingsDataService->getData()->getSearchByName()) {
            case SearchHelper::SEARCH_BY_CIVIL_NAME:
                $searchData->setCivilName('Dupont');
                break;
            default:
                $searchData->setUseName('Dupont');
        }

        $searchData->setBirthDate(\DateTime::createFromFormat('j/m/Y', '07/08/1986'));

        $personRepository = $this->entityManager->getRepository(Person::class);

        $controller = new SearchController($this->settingsDataService, $this->translator, 'default');

        /**
         * @var string $searchStatus
         * @var Person $person
         */
        [$searchStatus, $person] = $this->invokeMethod(
            $controller,
            'searchPerson',
            [$personRepository, $searchData, $useSoundex]
        );

        $this->assertSame(SearchHelper::SEARCH_STATUS_NOTFIND, $searchStatus);
        $this->assertSame(null, $person);
    }

    /**
     * @param bool $useSoundex
     *
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \ReflectionException
     */
    private function searchAmbiguousPerson(bool $useSoundex)
    {
        $this->initSettingsDataService();
        $searchData = new SearchData();

        $searchData->setDeathDate(\DateTime::createFromFormat('j/m/Y', '10/02/2020'));
        $searchData->setDeathLocation('VILLEFRANCHE');
        $searchData->setDeathCertificateDate(\DateTime::createFromFormat('j/m/Y', '10/02/2020'));
        $searchData->setFirstName('Clara');

        switch ($this->settingsDataService->getData()->getSearchByName()) {
            case SearchHelper::SEARCH_BY_CIVIL_NAME:
                $searchData->setCivilName('BOURDIAU');
                break;
            default:
                $searchData->setUseName('BOURDIAU');
        }

        $searchData->setBirthDate(\DateTime::createFromFormat('j/m/Y', '01/09/1966'));

        $personRepository = $this->entityManager->getRepository(Person::class);

        $controller = new SearchController($this->settingsDataService, $this->translator, 'default');

        /**
         * @var string $searchStatus
         * @var Person $person
         */
        [$searchStatus, $person] = $this->invokeMethod(
            $controller,
            'searchPerson',
            [$personRepository, $searchData, $useSoundex]
        );

        $this->assertSame(SearchHelper::SEARCH_STATUS_AMBIGUOUS, $searchStatus);
        $this->assertSame(null, $person);
    }

    /**
     * @param bool $useSoundex
     *
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \ReflectionException
     */
    private function searchNotFindRequestInProgressPerson(bool $useSoundex)
    {
        $this->initSettingsDataService();
        $searchData = new SearchData();

        $searchData->setDeathDate(\DateTime::createFromFormat('j/m/Y', '10/02/2020'));
        $searchData->setDeathLocation('VILLEFRANCHE');
        $searchData->setDeathCertificateDate(\DateTime::createFromFormat('j/m/Y', '10/02/2020'));
        $searchData->setFirstName('Yvette');

        switch ($this->settingsDataService->getData()->getSearchByName()) {
            case SearchHelper::SEARCH_BY_CIVIL_NAME:
                $searchData->setCivilName('ROSA');
                break;
            default:
                $searchData->setUseName('ROSA');
        }

        $searchData->setBirthDate(\DateTime::createFromFormat('j/m/Y', '08/12/1968'));

        $personRepository = $this->entityManager->getRepository(Person::class);

        $controller = new SearchController($this->settingsDataService, $this->translator, 'default');

        /**
         * @var string $searchStatus
         * @var Person $person
         */
        [$searchStatus, $person] = $this->invokeMethod(
            $controller,
            'searchPerson',
            [$personRepository, $searchData, $useSoundex]
        );

        $nameField = $this->settingsDataService->getData()->getSearchByName() === SearchHelper::SEARCH_BY_USE_NAME ?
            'useName' : 'civilName';

        $expectedPerson = $personRepository->findOneBy(
            [
                'firstName' => 'Yvette',
                $nameField  => 'ROSA',
                'birthDate' => \DateTime::createFromFormat('j/m/Y', '08/12/1968'),
            ]
        );

        $countPersons = $personRepository->countBySearch($searchData, false, false, false, true);

        $this->assertSame(SearchHelper::SEARCH_STATUS_NOTFIND, $searchStatus);
        $this->assertSame($expectedPerson, $person);
        $this->assertSame('', $person->getHelpCode());
        $this->assertSame(1, $countPersons);
    }

    /**
     * @param bool $needSendDeathAlert
     *
     * @throws \ReflectionException
     */
    private function searchDeathAlertPerson(bool $needSendDeathAlert = true)
    {
        $this->settingsData->setDeathAlert(true);
        $this->settingsData->setDeathAlertMail('notaires@deptnot.test');
        $this->initSettingsDataService();
        $searchData = new SearchData();

        if ($needSendDeathAlert) {
            $searchData->setDeathDate(\DateTime::createFromFormat('j/m/Y', '15/05/2019'));
        } else {
            $searchData->setDeathDate(\DateTime::createFromFormat('j/m/Y', '13/05/2019'));
        }

        $searchData->setDeathLocation('VILLEFRANCHE');
        $searchData->setDeathCertificateDate(\DateTime::createFromFormat('j/m/Y', '10/02/2020'));
        $searchData->setFirstName('Adrienne');

        switch ($this->settingsDataService->getData()->getSearchByName()) {
            case SearchHelper::SEARCH_BY_CIVIL_NAME:
                $searchData->setCivilName('GENILLON');
                $nameField = 'civilName';
                break;
            default:
                $searchData->setUseName('GENILLON');
                $nameField = 'useName';
        }

        $searchData->setBirthDate(\DateTime::createFromFormat('j/m/Y', '26/02/2001'));

        $personRepository = $this->entityManager->getRepository(Person::class);
        $findPerson = $personRepository->findOneBy(
            [
                'firstName' => 'Adrienne',
                $nameField  => 'GENILLON',
                'birthDate' => \DateTime::createFromFormat('j/m/Y', '26/02/2001'),
            ]
        );

        $controller = new SearchController($this->settingsDataService, $this->translator, 'default');

        /**
         * @var string $searchStatus
         * @var Person $person
         */
        $hasDeathAlert = $this->invokeMethod(
            $controller,
            'sendDeathAlert',
            [$findPerson, $searchData]
        );

        $this->assertSame(true, $hasDeathAlert);
    }

    /**
     * Call protected/private method of a class.
     *
     * @param object &$object     Instantiated object that we will run method on.
     * @param string  $methodName Method name to call
     * @param array   $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     * @throws \ReflectionException
     */
    private function invokeMethod(object &$object, string $methodName, array $parameters = [])
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
