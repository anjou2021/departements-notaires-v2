// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
Cypress.Commands.add('loginByCSRF', (username, password, csrfToken) => {
    cy.request({
        method:           'POST',
        url:              '/login',
        failOnStatusCode: false, // dont fail so we can make assertions
        form:             true, // we are submitting a regular form body
        body:             {
            username:    username,
            password:    password,
            _csrf_token: csrfToken, // insert this as part of form body
        },
    })
})

Cypress.Commands.add('login', (username, password) => {
    cy.request('/login')
        .its('body')
        .then((body) => {
            const $html = Cypress.$(body)
            const csrf = $html.find('input[name=_csrf_token]').val()

            cy.loginByCSRF(username, password, csrf)
                .then((resp) => {
                    expect(resp.status).to.eq(200)
                    expect(resp.body).to.include('<h1>Mes actions administrateur</h1>')
                })
        })

    Cypress.Cookies.preserveOnce('PHPSESSID')
})

Cypress.Commands.add('logout', () => {
    cy.visit('/logout')
})

Cypress.Commands.add('fillSearch', (values) => {
    cy.get('input#search_agreeTerms').check()
    cy.get('input#search_deathDate').type(values.deathDate)
    cy.get('input#search_deathLocation').type(values.deathLocation)
    cy.get('input#search_deathCertificateDate').type(values.deathCertificateDate)
    cy.get('input#search_firstName').type(values.firstName)
    cy.get('input#search_useName').type(values.useName)
    cy.get('input#search_birthDate').type(values.birthDate)
    cy.get('form[name=search]').submit()
})

Cypress.Commands.add('fillUser', (values) => {
    cy.get('select#user_roles').select(values.role)
    cy.wait(2000)
    cy.get('input#user_username').type(values.username)
    cy.get('input#user_email').type(values.email)
    cy.get('input#user_name').type(values.name)
    if (values.address) {
        cy.get('input#user_address').type(values.address)
    }
    if (values.additionalAddress) {
        cy.get('input#user_additionalAddress').type(values.additionalAddress)
    }
    if (values.postalCode) {
        cy.get('input#user_postalCode').type(values.postalCode)
    }
    if (values.town) {
        cy.get('input#user_town').type(values.town)
    }
    if (values.service) {
        cy.get('input#user_service').type(values.service)
    }
    cy.get('form[name=user]').submit()
})

Cypress.Commands.add('fillInstructor', (values) => {
    cy.get('input#instructor_firstName').type(values.firstName)
    cy.get('input#instructor_lastName').type(values.lastName)
    cy.get('input#instructor_email').type(values.email)
    if (values.phone) {
        cy.get('input#instructor_phone').type(values.phone)
    }
    values.responseTypes.forEach((responseType) => {
        cy.get('input#instructor_responseType_' + responseType).check().should('be.checked')
    })
    if (values.initials) {
        cy.get('input#instructor_initials').type(values.initials)
    }

    cy.get('form[name=instructor]').submit()
})

// Vérifie qu'un mail a bien été envoyé à un destinataire donné (le mail doit être envoyé qu'une fois)
Cypress.Commands.add('checkMailReceiver', email => {
    cy.request({
        method:  'GET',
        url:     Cypress.env('mailhogUrl') + '/api/v2/search',
        headers: {
            'content-type': 'application/json',
        },
        qs:      {
            kind:  'to',
            query: decodeURIComponent(email)
        },
        json:    true,
    }).then((response) => {
        const items = response.body.items

        return items.length === 1;
    })
});

Cypress.Commands.add('deleteAllMails', email => {
    cy.request({
        method:  'DELETE',
        url:     Cypress.env('mailhogUrl') + '/api/v1/messages',
        headers: {
            'content-type': 'application/json',
        },
        json:    true,
    }).then((response) => {
        assert.equal(response.status, 200)
    })
});

Cypress.Commands.add('checkMailContent', (email, content) => {
    cy.request({
        method:  'GET',
        url:     Cypress.env('mailhogUrl') + '/api/v2/search',
        headers: {
            'content-type': 'application/json',
        },
        qs:      {
            kind:  'to',
            query: decodeURIComponent(email)
        },
        json:    true,
    }).then((response) => {
        const items = response.body.items

        if (items.length === 1) {
            let mail = items[0]
            let regExpContent = new RegExp(content, "g");

            assert.match(mail.Content.Body, regExpContent)

            return true;
        }

        return false;
    })
});
