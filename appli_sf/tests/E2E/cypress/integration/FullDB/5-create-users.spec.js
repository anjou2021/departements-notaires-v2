describe('Création d\'utilisateurs', () => {
    if (Cypress.env('ENVIRONMENT') === 'dev') {
        before(() => {
            // On rempli la base de données
            cy.exec("cd /srv && docker-compose exec -T php /entrypoint ./bin/console doctrine:fixture:load --no-interaction --group=use_name")
        })
    }

    beforeEach(function () {
        cy.fixture('user-admin')
            .then((user) => {
                this.userAdmin = user
            })
    })

    it('Login', function () {
        cy.login(this.userAdmin.username, this.userAdmin.password)
    })

    it('Création d\'un utilisateur avec profil admin', () => {
        cy.visit('/admin/user/add')
        cy.fillUser({
            role:              'ROLE_ADMIN',
            username:          'admin-test',
            email:             'admin-test@notaire.fr',
            name:              'Compte admin de test'
        })

        cy.contains('Gérer les utilisateurs')
        cy.get('table.users-table tr').last().find('td').eq(3).should('have.text', 'admin-test')
        cy.get('table.users-table tr').last().find('td').eq(4).contains('admin-test@notaire.fr')
        cy.get('table.users-table tr').last().find('td').eq(6).should('have.text', 'Administrateur')
    })

    it('Création d\'un utilisateur avec profil agent', () => {
        cy.visit('/admin/user/add')
        cy.fillUser({
            role:              'ROLE_AGENT',
            username:          'agent-test',
            email:             'agent-test@notaire.fr',
            name:              'Compte agent de test'
        })

        cy.contains('Gérer les utilisateurs')
        cy.get('table.users-table tr').last().find('td').eq(3).should('have.text', 'agent-test')
        cy.get('table.users-table tr').last().find('td').eq(4).contains('agent-test@notaire.fr')
        cy.get('table.users-table tr').last().find('td').eq(6).should('have.text', 'Agent du département')
    })

    it('Création d\'un utilisateur avec profil notaire', () => {
        cy.visit('/admin/user/add')
        cy.fillUser({
            role:              'ROLE_NOTARY',
            username:          '1',
            email:             'etude-test@notaire.fr',
            name:              'étude',
            address:           'Place de la République',
            additionalAddress: '2° étage',
            postalCode:        '34000',
            town:              'Montpellier'
        })

        cy.contains('Gérer les utilisateurs')
        cy.get('table.users-table tr').last().find('td').eq(3).should('have.text', '1')
        cy.get('table.users-table tr').last().find('td').eq(4).contains('etude-test@notaire.fr')
        cy.get('table.users-table tr').last().find('td').eq(6).should('have.text', 'Etude notariale')
    })

    it('Déconnexion', () => {
        cy.logout()
    })
})
