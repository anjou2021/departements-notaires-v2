describe('Page des stats', () => {
    if (Cypress.env('ENVIRONMENT') === 'dev') {
        before(() => {
            // On rempli la base de données
            cy.exec("cd /srv && docker-compose exec -T php /entrypoint ./bin/console doctrine:fixture:load --no-interaction --group=use_name")
        })
    }

    beforeEach(function () {
        cy.fixture('user-admin')
            .then((user) => {
                this.userAdmin = user
            })
    })

    it('Login', function () {
        cy.login(this.userAdmin.username, this.userAdmin.password)
    })

    it('Affichage 5 derniers mois', () => {
        Cypress.moment.locale('fr')
        const monthFormat = 'MMMM-YY'
        const currentMonth = Cypress.moment().startOf('month')

        cy.visit('/stats')

        // Nombre de mois affichés
        cy.get('table.stats-table thead th').eq(1).should('have.text', currentMonth.format(monthFormat))
        cy.get('table.stats-table thead th').eq(2).should('have.text', currentMonth.subtract(1, 'months').format(monthFormat))
        cy.get('table.stats-table thead th').eq(3).should('have.text', currentMonth.subtract(1, 'months').format(monthFormat))
        cy.get('table.stats-table thead th').eq(4).should('have.text', currentMonth.subtract(1, 'months').format(monthFormat))
        cy.get('table.stats-table thead th').eq(5).should('have.text', currentMonth.subtract(1, 'months').format(monthFormat))
    })

    it('Incrémentation nombre de recherches', () => {
        cy.visit('/stats')

        // Nombre de réponses
        cy.get('table.stats-table tbody').then((statsTableBody) => {
            const nbResponses = parseInt(statsTableBody.find('tr').eq(1).find('td').eq(1).text().trim())
            const nbResponsesRecup = parseInt(statsTableBody.find('tr').eq(2).find('td').eq(1).text().trim())

            cy.visit('/search')
            cy.fillSearch({
                deathDate:            '10/02/2020',
                deathLocation:        'VILLEFRANCHE',
                deathCertificateDate: '12/02/2020',
                firstName:            'Aimé',
                useName:              'OLMO',
                birthDate:            '06/12/1977'
            })

            cy.visit('/stats')
            cy.get('table.stats-table tbody tr').eq(1).find('td').eq(1).contains(nbResponses + 1)
            cy.get('table.stats-table tbody tr').eq(2).find('td').eq(1).contains(nbResponsesRecup + 1)
        })
    })

    it('Déconnexion', () => {
        cy.logout()
    })
})
