describe('Création d\'instructeurs', () => {
    if (Cypress.env('ENVIRONMENT') === 'dev') {
        before(() => {
            // On rempli la base de données
            cy.exec("cd /srv && docker-compose exec -T php /entrypoint ./bin/console doctrine:fixture:load --no-interaction --group=use_name")
        })
    }

    beforeEach(function () {
        cy.fixture('user-admin')
            .then((user) => {
                this.userAdmin = user
            })
        cy.deleteAllMails()
    })

    it('Login', function () {
        cy.login(this.userAdmin.username, this.userAdmin.password)
    })

    it('Création d\'un premier instructeur', () => {
        cy.visit('/admin/instructor/add')
        cy.fillInstructor({
            firstName:     'instructeur',
            lastName:      'AMBIGU',
            email:         'instructeur1@notaire.fr',
            phone:         '06 06 06 06 06',
            responseTypes: [2],
            initials:      'A-Z'
        })

        cy.contains('Gérer les instructeurs')
        cy.get('table.users-table tr').last().find('td').eq(2).should('have.text', 'instructeur')
        cy.get('table.users-table tr').last().find('td').eq(3).should('have.text', 'AMBIGU')
        cy.get('table.users-table tr').last().find('td').eq(4).contains('instructeur1@notaire.fr')
        cy.get('table.users-table tr').last().find('td').eq(6).contains('Ambigu')
        cy.get('table.users-table tr').last().find('td').eq(7).should('have.text', 'A-Z')
    })

    it('Recherche d\'une personne avec résultat ambigu', () => {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Clara',
            useName:              'BOURDIAU',
            birthDate:            '01/09/1966'
        })

        cy.get('body').should('not.contain', 'Nous n\'avons pas trouvé d\'instructeur associé à votre requête')
        cy.checkMailReceiver('instructeur1@notaire.fr').should('be.true')
    })

    it('Création d\'un second instructeur', () => {
        cy.visit('/admin/instructor/add')
        cy.fillInstructor({
            firstName:     'instructeur',
            lastName:      'RECUPERATION',
            email:         'instructeur2@notaire.fr',
            phone:         '06 06 06 06 06',
            responseTypes: [0],
            initials:      'A-Z'
        })

        cy.contains('Gérer les instructeurs')
        cy.get('table.users-table tr').last().find('td').eq(2).should('have.text', 'instructeur')
        cy.get('table.users-table tr').last().find('td').eq(3).should('have.text', 'RECUPERATION')
        cy.get('table.users-table tr').last().find('td').eq(4).contains('instructeur2@notaire.fr')
        cy.get('table.users-table tr').last().find('td').eq(6).contains('Récupération')
        cy.get('table.users-table tr').last().find('td').eq(7).should('have.text', 'A-Z')
    })

    it('Recherche d\'une personne avec récupération', () => {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Aime',
            useName:              'OLMO',
            birthDate:            '06/12/1977'
        })

        cy.get('body').should('not.contain', 'Nous n\'avons pas trouvé d\'instructeur associé à votre requête')
        cy.checkMailReceiver('instructeur2@notaire.fr').should('be.true')
    })

    it('Création d\'un instructeur multi-réponses et initiales communes avec un autre instructeur', () => {
        cy.visit('/admin/instructor/add')
        cy.fillInstructor({
            firstName:     'instructeur',
            lastName:      'AMBIGU2',
            email:         'instructeur3@notaire.fr',
            phone:         '06 06 06 06 06',
            responseTypes: [0,2],
            initials:      'M-Z'
        })

        cy.contains('Gérer les instructeurs')
        cy.get('table.users-table tr').last().find('td').eq(2).should('have.text', 'instructeur')
        cy.get('table.users-table tr').last().find('td').eq(3).should('have.text', 'AMBIGU2')
        cy.get('table.users-table tr').last().find('td').eq(4).contains('instructeur3@notaire.fr')
        cy.get('table.users-table tr').last().find('td').eq(6).contains('Récupération')
        cy.get('table.users-table tr').last().find('td').eq(6).contains('Ambigu')
        cy.get('table.users-table tr').last().find('td').eq(7).should('have.text', 'M-Z')
    })

    it('Recherche d\'une personne avec résultat ambigu', () => {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Clara',
            useName:              'BOURDIAU',
            birthDate:            '01/09/1966'
        })

        cy.get('body').should('not.contain', 'Nous n\'avons pas trouvé d\'instructeur associé à votre requête')
        cy.checkMailReceiver('instructeur1@notaire.fr').should('be.true')
        cy.checkMailReceiver('instructeur3@notaire.fr').should('be.false')
    })

    it('Déconnexion', () => {
        cy.logout()
    })
})
