describe('Recherche', () => {
    if (Cypress.env('ENVIRONMENT') === 'dev') {
        before(() => {
            // On rempli la base de données
            cy.exec("cd /srv && docker-compose exec -T php /entrypoint ./bin/console doctrine:fixture:load --no-interaction --group=use_name")
        })
    }

    beforeEach(function () {
        cy.fixture('user-admin')
            .then((user) => {
                this.userAdmin = user
            })
        cy.deleteAllMails()
    })

    it('Login', function () {
        cy.login(this.userAdmin.username, this.userAdmin.password)
    })

    it('Personne connue, aide récupérable', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Aimé',
            useName:              'OLMO',
            birthDate:            '06/12/1977'
        })

        cy.contains('Cette personne est connue de nos services')
        cy.checkMailContent(this.userAdmin.email, "connue").should('be.true')
    })

    it('Personne connue, aide non récupérable', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Maesson',
            useName:              'ALOUANE',
            birthDate:            '03/12/1954'
        })

        cy.contains('Cette personne est connue de nos services')
        cy.checkMailContent(this.userAdmin.email, "connue").should('be.true')
    })

    it('Personne inconnue', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Mickael',
            useName:              'Dupont',
            birthDate:            '07/08/1986'
        })

        cy.contains('Cette personne est inconnue de nos services')
        cy.checkMailContent(this.userAdmin.email, "<strong>inconnue</strong>").should('be.true')
    })

    it('Personne inconnue, demande en cours d\'instruction', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Yvette',
            useName:              'ROSA',
            birthDate:            '08/12/1968'
        })

        cy.contains('Cette personne est inconnue de nos services')
        cy.checkMailContent(this.userAdmin.email, "il existe une demande en cours").should('be.true')
    })

    it('Resultat ambigu', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Clara',
            useName:              'BOURDIAU',
            birthDate:            '01/09/1966'
        })

        cy.contains('Les éléments que vous avez saisis ne permettent pas d\'établir avec certitude l\'identité')
        cy.checkMailContent(this.userAdmin.email, "avec certitude").should('be.true')
    })

    it('Déconnexion', () => {
        cy.logout()
    })
})
