<?php

namespace App\DataFixtures;

use App\Helper\FixturesHelper;
use App\Helper\SearchHelper;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;

class PersonCivilNameFixtures extends AbstractPersonFixtures implements FixtureGroupInterface
{
    protected $nameToUse = SearchHelper::SEARCH_BY_CIVIL_NAME;

    public static function getGroups(): array
    {
        return [FixturesHelper::GROUP_CIVIL_NAME];
    }
}
