<?php
// src/AppBundle/Entity/User.php

namespace App\Entity;

use App\Helper\UserHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Rollerworks\Component\PasswordStrength\Validator\Constraints as RollerworksPassword;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="membre")
 *
 * @UniqueEntity("username")
 * @UniqueEntity("email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="id")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=200, nullable=false, unique=true)
     */
    protected $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     * @RollerworksPassword\PasswordRequirements(minLength=12, requireCaseDiff=true, requireNumbers=true,
     *                                                         requireSpecialCharacter=true)
     */
    protected $password;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mail", type="string", length=255, nullable=false, unique=true)
     */
    protected $email;

    /**
     * @var array
     *
     * @ORM\Column(name="role", type="json", nullable=false)
     */
    protected $roles;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="nom", nullable = false)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="service", nullable = true)
     */
    protected $service;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="adresse", nullable = true)
     */
    protected $address;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="complement_adresse", nullable = true)
     */
    protected $additionalAddress;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="code_postal", nullable = true)
     */
    protected $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="ville", nullable = true)
     */
    protected $town;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="nb_log_error", options={"default" : 0})
     */
    protected $nbLoginAttempts = 0;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="token_reinit", nullable = true)
     */
    protected $tokenReset;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", name="desactive", nullable = false)
     */
    protected $disabled = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserLog", mappedBy="user")
     */
    private $userLogs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SearchLog", mappedBy="user", orphanRemoval=true)
     */
    private $searchLogs;

    public function __construct()
    {
        $this->userLogs = new ArrayCollection();
        $this->searchLogs = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @see UserInterface
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getService(): ?string
    {
        return $this->service;
    }

    public function setService(?string $service): self
    {
        $this->service = $service;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getAdditionalAddress(): ?string
    {
        return $this->additionalAddress;
    }

    public function setAdditionalAddress(?string $additionalAddress): self
    {
        $this->additionalAddress = $additionalAddress;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getTown(): ?string
    {
        return $this->town;
    }

    public function setTown(?string $town): self
    {
        $this->town = $town;

        return $this;
    }

    public function getNbLoginAttempts(): int
    {
        return $this->nbLoginAttempts;
    }

    public function setNbLoginAttempts(int $nbLoginAttempts): self
    {
        $this->nbLoginAttempts = $nbLoginAttempts;

        return $this;
    }

    public function getTokenReset(): ?string
    {
        return $this->tokenReset;
    }

    public function setTokenReset(?string $tokenReset): self
    {
        $this->tokenReset = $tokenReset;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    /**
     * @param bool $disabled
     *
     * @return User
     */
    public function setDisabled(bool $disabled): User
    {
        $this->disabled = $disabled;

        return $this;
    }

    /**
     * @return string|null
     * @see UserInterface
     *
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function incrementLoginAttempts()
    {
        $this->nbLoginAttempts++;
    }

    /**
     * @return Collection|UserLog[]
     */
    public function getUserLogs(): Collection
    {
        return $this->userLogs;
    }

    public function isAdmin()
    {
        if (in_array(UserHelper::ROLE_ADMIN, $this->roles)) {
            return true;
        }

        return false;
    }

    public function isNotary()
    {
        if (in_array(UserHelper::ROLE_NOTARY, $this->roles)) {
            return true;
        }

        return false;
    }

    /**
     * @return Collection|SearchLog[]
     */
    public function getSearchLogs(): Collection
    {
        return $this->searchLogs;
    }

    public function addSearchLog(SearchLog $searchLog): self
    {
        if (!$this->searchLogs->contains($searchLog)) {
            $this->searchLogs[] = $searchLog;
            $searchLog->setUser($this);
        }

        return $this;
    }

    public function removeSearchLog(SearchLog $searchLog): self
    {
        if ($this->searchLogs->contains($searchLog)) {
            $this->searchLogs->removeElement($searchLog);
            // set the owning side to null (unless already changed)
            if ($searchLog->getUser() === $this) {
                $searchLog->setUser(null);
            }
        }

        return $this;
    }
}
