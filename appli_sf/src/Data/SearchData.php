<?php

namespace App\Data;

use App\Entity\AbstractPerson;
use App\Entity\SearchLog;
use DateTime;

class SearchData extends AbstractPerson
{
    /**
     * @var DateTime
     */
    protected $deathDate;

    /**
     * @var string
     */
    protected $deathLocation;

    /**
     * @var DateTime
     */
    protected $deathCertificateDate;

    /**
     * @var string|null
     */
    protected $recipient;

    /**
     * @var string
     */
    protected $firstName;

    /**
     * @var string|null
     */
    protected $middleName;

    /**
     * @var string|null
     */
    protected $thirdName;

    /**
     * @var string
     */
    protected $useName;

    /**
     * @var string|null
     */
    protected $civilName;

    /**
     * @var DateTime
     */
    protected $birthDate;

    /**
     * @return DateTime|null
     */
    public function getDeathDate(): ?DateTime
    {
        return $this->deathDate;
    }

    /**
     * @param DateTime $deathDate
     *
     * @return SearchData
     */
    public function setDeathDate(DateTime $deathDate): SearchData
    {
        $this->deathDate = $deathDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getDeathLocation(): ?string
    {
        return $this->deathLocation;
    }

    /**
     * @param string $deathLocation
     *
     * @return SearchData
     */
    public function setDeathLocation(string $deathLocation): SearchData
    {
        $this->deathLocation = $deathLocation;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDeathCertificateDate(): ?DateTime
    {
        return $this->deathCertificateDate;
    }

    /**
     * @param DateTime $deathCertificateDate
     *
     * @return SearchData
     */
    public function setDeathCertificateDate(DateTime $deathCertificateDate): SearchData
    {
        $this->deathCertificateDate = $deathCertificateDate;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRecipient(): ?string
    {
        return $this->recipient;
    }

    /**
     * @param string|null $recipient
     *
     * @return SearchData
     */
    public function setRecipient(?string $recipient): SearchData
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     *
     * @return SearchData
     */
    public function setFirstName(string $firstName): SearchData
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    /**
     * @param string|null $middleName
     *
     * @return SearchData
     */
    public function setMiddleName(?string $middleName): SearchData
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getThirdName(): ?string
    {
        return $this->thirdName;
    }

    /**
     * @param string|null $thirdName
     *
     * @return SearchData
     */
    public function setThirdName(?string $thirdName): SearchData
    {
        $this->thirdName = $thirdName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUseName(): ?string
    {
        return $this->useName;
    }

    /**
     * @param string $useName
     *
     * @return SearchData
     */
    public function setUseName(string $useName): SearchData
    {
        $this->useName = $useName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCivilName(): ?string
    {
        return $this->civilName;
    }

    /**
     * @param string|null $civilName
     *
     * @return SearchData
     */
    public function setCivilName(?string $civilName): SearchData
    {
        $this->civilName = $civilName;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getBirthDate(): ?DateTime
    {
        return $this->birthDate;
    }

    /**
     * @param DateTime $birthDate
     *
     * @return SearchData
     */
    public function setBirthDate(DateTime $birthDate): SearchData
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * @return array
     */
    public function getAllNamesWithoutHyphens(): array
    {
        $names = [];

        if ($this->useName) {
            $names[] = $this->useName;
            $names[] = str_replace('-', ' ', $this->useName);
        }

        if ($this->civilName) {
            $names[] = $this->civilName;
            $names[] = str_replace('-', ' ', $this->civilName);
        }

        return $names;
    }

    /**
     * @return array
     */
    public function getAllNames(): array
    {
        $names = [];

        if ($this->useName) {
            $names[] = $this->useName;
        }

        if ($this->civilName) {
            $names[] = $this->civilName;
        }

        return $names;
    }

    /**
     * @return array
     */
    public function getAllFirstNames(): array
    {
        $names = [];

        if ($this->firstName) {
            $names[] = $this->firstName;
        }

        if ($this->middleName) {
            $names[] = $this->middleName;
        }

        if ($this->thirdName) {
            $names[] = $this->thirdName;
        }

        return $names;
    }

    /**
     * @return false|string
     */
    public function getFirstNameStart()
    {
        return substr($this->firstName, 0, 3);
    }

    /**
     * @return SearchLog
     * @throws \Exception
     */
    public function toSearchLog()
    {
        $searchLog = new SearchLog();

        $searchLog->setUseName($this->useName);
        $searchLog->setCivilName($this->civilName);
        $searchLog->addFirstName($this->firstName);
        if ($this->middleName) {
            $searchLog->addFirstName($this->middleName);
        }
        if ($this->thirdName) {
            $searchLog->addFirstName($this->thirdName);
        }
        $searchLog->setBirthDate($this->birthDate);
        $searchLog->setDeathDate($this->deathDate);
        $searchLog->setDeathLocation($this->deathLocation);
        $searchLog->setDeathCertificateDate($this->deathCertificateDate);

        return $searchLog;
    }
}
