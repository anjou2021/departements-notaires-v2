<?php

namespace App\Repository;

use App\Entity\SearchLog;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SearchLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method SearchLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method SearchLog[]    findAll()
 * @method SearchLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SearchLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SearchLog::class);
    }

    /**
     * Liste les noms uniques (civil ou usage)
     *
     * @param User|null $user
     *
     * @return array
     */
    public function findNames(?User $user): array
    {
        // Liste des noms d'usage uniques
        $queryBuilder = $this->createQueryBuilder('s')
            ->groupBy('s.useName')
            ->orderBy('s.useName', 'ASC');

        if ($user) {
            $queryBuilder->andWhere('s.user = :user')
                ->setParameter(':user', $user);
        }

        $useNames = $queryBuilder->getQuery()->getResult();

        // Liste des noms civil uniques
        $queryBuilder = $this->createQueryBuilder('s')
            ->groupBy('s.civilName')
            ->orderBy('s.civilName', 'ASC');

        if ($user) {
            $queryBuilder->andWhere('s.user = :user')
                ->setParameter(':user', $user);
        }

        $civilNames = $queryBuilder->getQuery()->getResult();

        return array_merge($useNames, $civilNames);
    }

    /**
     * Liste les logs selon les critères de filtrage
     *
     * @param array     $filterData
     * @param User|null $user
     *
     * @return int|mixed|string
     */
    public function findByFilter(array $filterData, ?User $user)
    {
        $queryBuilder = $this->createQueryBuilder('s');

        if (!$user && array_key_exists('user', $filterData) && $filterData['user'] instanceof User) {
            $user = $filterData['user'];
        }

        if ($user) {
            $queryBuilder->andWhere('s.user = :user')
                ->setParameter(':user', $user);
        }

        if ($filterData['responseType']) {
            $queryBuilder->andWhere('s.responseType = :responseType')
                ->setParameter('responseType', $filterData['responseType']);
        }

        if ($filterData['name']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->eq('s.useName', ':name'),
                    $queryBuilder->expr()->eq('s.civilName', ':name')
                )
            )
                ->setParameter('name', $filterData['name']);
        }

        if ($filterData['startDate'] instanceof \DateTime) {
            $queryBuilder->andWhere('s.searchDate >= :startDate')
                ->setParameter('startDate', $filterData['startDate']);
        }

        if ($filterData['endDate'] instanceof \DateTime) {
            $queryBuilder->andWhere('s.searchDate <= :endDate')
                ->setParameter('endDate', $filterData['endDate']);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * Récupère les stats de recherche (nombre de recherche pas type de réponse)
     * pour chaque mois dans un interval de temps
     *
     * @param \DateTime $dateFrom
     * @param \DateTime $dataTo
     *
     * @return mixed
     */
    public function findSearchStats(\DateTime $dateFrom, \DateTime $dataTo)
    {
        return $this->createQueryBuilder('s')
            ->select("DATE_FORMAT(s.searchDate, '%Y-%m') as month", "s.responseType", "COUNT(s.responseType) as nb")
            ->andWhere('s.searchDate >= :dateFrom')
            ->andWhere('s.searchDate <= :dateTo')
            ->groupBy('month', 's.responseType')
            ->orderBy('month', 'DESC')
            ->orderBy('s.responseType', 'ASC')
            ->setParameters(
                [
                    'dateFrom' => $dateFrom,
                    'dateTo'   => $dataTo,
                ]
            )
            ->getQuery()
            ->getResult();
    }
}
