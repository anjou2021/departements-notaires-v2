<?php

namespace App\Repository;

use App\Entity\UserLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method UserLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserLog[]    findAll()
 * @method UserLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserLog::class);
    }

    /**
     * Récupère le nombre de connexions d'utilisateurs par mois dans un interval donné
     *
     * @param \DateTime $dateFrom
     * @param \DateTime $dataTo
     *
     * @return mixed
     */
    public function countLogByMonth(\DateTime $dateFrom, \DateTime $dataTo)
    {
        return $this->createQueryBuilder('ul')
            ->select("DATE_FORMAT(ul.date, '%Y-%m') as month", "COUNT(ul.user) as nb")
            ->andWhere('ul.date >= :dateFrom')
            ->andWhere('ul.date <= :dateTo')
            ->groupBy('month')
            ->orderBy('month', 'DESC')
            ->setParameters(
                [
                    'dateFrom' => $dateFrom,
                    'dateTo'   => $dataTo,
                ]
            )
            ->getQuery()
            ->getResult();
    }

    /**
     * Récupère le nombre de connexions uniques d'utilisateurs par mois dans un interval donné
     *
     * @param \DateTime $dateFrom
     * @param \DateTime $dataTo
     *
     * @return mixed
     */
    public function countUniqLogByMonth(\DateTime $dateFrom, \DateTime $dataTo)
    {
        return $this->createQueryBuilder('ul')
            ->select("DATE_FORMAT(ul.date, '%Y-%m') as month", "COUNT(DISTINCT(ul.user)) as nb")
            ->andWhere('ul.date >= :dateFrom')
            ->andWhere('ul.date <= :dateTo')
            ->groupBy('month')
            ->orderBy('month', 'DESC')
            ->setParameters(
                [
                    'dateFrom' => $dateFrom,
                    'dateTo'   => $dataTo,
                ]
            )
            ->getQuery()
            ->getResult();
    }
}
