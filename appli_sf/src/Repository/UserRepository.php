<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Recherche un utilisateur pour un rôle donné
     *
     * @param string $role
     *
     * @return int|null
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByRole(string $role)
    {
        return $this->createQueryBuilder('user')
            ->where('user.roles LIKE :role')
            ->setMaxResults(1)
            ->orderBy('user.id', 'ASC')
            ->setParameters(
                [
                    'role' => '%"'.$role.'"%',
                ]
            )
            ->getQuery()
            ->getSingleResult();
    }
}
