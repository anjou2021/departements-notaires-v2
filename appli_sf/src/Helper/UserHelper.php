<?php

namespace App\Helper;

class UserHelper
{
    const ROLE_AGENT  = 'ROLE_AGENT';
    const ROLE_NOTARY = 'ROLE_NOTARY';
    const ROLE_ADMIN  = 'ROLE_ADMIN';

    /**
     * Transforme le role d'un entier vers un string
     *
     * @param int $number
     *
     * @return string
     */
    public static function getRoleFromInt(int $number)
    {
        switch ($number) {
            case 3:
                return self::ROLE_ADMIN;
            case 2:
                return self::ROLE_AGENT;
            default:
                return self::ROLE_NOTARY;
        }
    }

    /**
     * Retourne la liste des types de réponse (format pour les select HTML)
     *
     * @return array|string[]
     */
    public static function getRolesChoices(): array
    {
        return [
            self::ROLE_ADMIN  => self::ROLE_ADMIN,
            self::ROLE_AGENT  => self::ROLE_AGENT,
            self::ROLE_NOTARY => self::ROLE_NOTARY,
        ];
    }
}
