<?php

namespace App\Helper;

use App\Entity\SearchLog;
use App\Entity\User;

class SearchHelper
{
    const HELP_CODE_REC    = '1SEXTREC';
    const HELP_CODE_NOTREC = '1SEXTNONR';

    const SEARCH_STATUS_FIND_REC    = 1;
    const SEARCH_STATUS_FIND_NOTREC = 2;
    const SEARCH_STATUS_NOTFIND     = 3;
    const SEARCH_STATUS_AMBIGUOUS   = 4;
    const SEARCH_STATUS_HAS_REQUEST = 5;

    const SEARCH_BY_USE_NAME = 1;
    const SEARCH_BY_CIVIL_NAME = 2;

    /**
     * Retourne la liste des types de réponse (format pour les select HTML)
     *
     * @return array|int[]
     */
    public static function getResponseTypesChoices(): array
    {
        return [
            'responseType.'.self::SEARCH_STATUS_FIND_REC    => self::SEARCH_STATUS_FIND_REC,
            'responseType.'.self::SEARCH_STATUS_FIND_NOTREC => self::SEARCH_STATUS_FIND_NOTREC,
            'responseType.'.self::SEARCH_STATUS_NOTFIND     => self::SEARCH_STATUS_NOTFIND,
            'responseType.'.self::SEARCH_STATUS_AMBIGUOUS   => self::SEARCH_STATUS_AMBIGUOUS,
        ];
    }

    /**
     * Retourne la liste des types de réponse pour les instructeurs (format pour les select HTML)
     *
     * @return array|int[]
     */
    public static function getResponseTypesChoicesForInstructor(): array
    {
        return [
            'responseType.'.self::SEARCH_STATUS_FIND_REC    => self::SEARCH_STATUS_FIND_REC,
            'responseType.'.self::SEARCH_STATUS_FIND_NOTREC => self::SEARCH_STATUS_FIND_NOTREC,
            'responseType.'.self::SEARCH_STATUS_AMBIGUOUS   => self::SEARCH_STATUS_AMBIGUOUS,
        ];
    }

    /**
     * Retourne la référence utilisé dans les PDF
     *
     * @param User      $user
     * @param SearchLog $searchLog
     *
     * @return string
     */
    public static function getReference(User $user, SearchLog $searchLog): string
    {
        return $user->getUsername().' '.$user->getName().'/Succession '.$searchLog->getFirstName().' '.
            $searchLog->getUseName();
    }

    /**
     * Détermine si le type de réponse nécessite l'envoie d'un PDF
     *
     * @param int $responseType
     *
     * @return bool
     */
    public static function hasLetterPdf(int $responseType): bool
    {
        switch ($responseType) {
            case self::SEARCH_STATUS_FIND_REC:
            case self::SEARCH_STATUS_FIND_NOTREC:
            case self::SEARCH_STATUS_NOTFIND:
                return true;
            default:
                return false;
        }
    }
}
