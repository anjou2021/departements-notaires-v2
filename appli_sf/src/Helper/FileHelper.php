<?php

namespace App\Helper;

class FileHelper
{
    /**
     * Retourne un nom de fichier sans caractères spéciaux
     *
     * @param string $filename
     *
     * @return string
     */
    public static function sanitizeFileName(string $filename): string
    {
        $dangerous_characters = [" ", '"', "'", "&", "/", "\\", "?", "#"];

        return str_replace($dangerous_characters, '_', $filename);
    }
}
