<?php

namespace App\Helper;

use Psr\Log\LoggerInterface;
use Symfony\Component\Asset\Context\RequestStackContext;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploaderHelper
{
    const SETTINGS_IMAGE = 'settings/images';
    const SETTINGS_FILE  = 'settings/files';

    /**
     * @var string
     */
    private $uploadsPath;

    /**
     * @var string
     */
    private $uploadedAssetsBaseUrl;

    /**
     * @var \Symfony\Component\Asset\Context\RequestStackContext
     */
    private $requestStackContext;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * UploaderHelper constructor.
     *
     * @param \Symfony\Component\Asset\Context\RequestStackContext $requestStackContext
     * @param \Psr\Log\LoggerInterface                             $logger
     * @param string                                               $uploadsPath
     * @param string                                               $uploadedAssetsBaseUrl
     */
    public function __construct(
        RequestStackContext $requestStackContext,
        LoggerInterface $logger,
        string $uploadsPath,
        string $uploadedAssetsBaseUrl
    ) {
        $this->uploadsPath = $uploadsPath;
        $this->uploadedAssetsBaseUrl = $uploadedAssetsBaseUrl;
        $this->requestStackContext = $requestStackContext;
        $this->logger = $logger;
    }

    /**
     * Upload d'un fichier
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $uploadedFile
     *
     * @return \Symfony\Component\HttpFoundation\File\File
     */
    public function uploadFile(UploadedFile $uploadedFile, string $fileDir): File
    {
        $destination = $this->uploadsPath.'/'.$fileDir;

        $originalFilename = pathinfo($uploadedFile->getClientOriginalName());
        $safeFilename = \transliterator_transliterate(
            'Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()',
            $originalFilename['filename']
        );

        $newFilename = $safeFilename.'-'.uniqid().'.'.$originalFilename['extension'];

        return $uploadedFile->move(
            $destination,
            $newFilename
        );
    }

    /**
     * Retourne le chemin d'accès public à un fichier
     *
     * @param \Symfony\Component\HttpFoundation\File\File $file
     *
     * @return string
     */
    public function getPublicPath(File $file): string
    {
        $path = $this->getRelativePath($file);

        return '/'.$this->uploadedAssetsBaseUrl.'/'.$path;
    }

    /**
     * Retourne le chemin relatif d'accès à un fichier
     *
     * @param \Symfony\Component\HttpFoundation\File\File $file
     *
     * @return string
     */
    public function getRelativePath(File $file): string
    {
        return str_replace($this->uploadsPath.'/', '', $file->getPathname());
    }

    /**
     * Retourne le chemin sur le serveur
     *
     * @param string $path
     *
     * @return string
     */
    public function getServerPath(string $path): string
    {
        return $this->requestStackContext
                ->getBasePath().$this->uploadsPath.'/'.$path;
    }
}
