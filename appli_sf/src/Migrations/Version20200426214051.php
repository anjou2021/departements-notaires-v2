<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200426214051 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE individu ADD num_ind INT NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5EE42FCE1961CB7B ON individu (num_ind)');
        $this->addSql('CREATE INDEX num_ind ON individu (num_ind)');
        $this->addSql('ALTER TABLE log_recherche DROP FOREIGN KEY FK_A22587C8E3FC35B');
        $this->addSql('DROP INDEX IDX_A22587C8E3FC35B ON log_recherche');
        $this->addSql('ALTER TABLE log_recherche CHANGE id_individu id_individu INT NOT NULL');
        $this->addSql('ALTER TABLE log_recherche CHANGE id_individu id_individu INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_5EE42FCE1961CB7B ON individu');
        $this->addSql('DROP INDEX num_ind ON individu');
        $this->addSql('ALTER TABLE individu DROP num_ind');
        $this->addSql('ALTER TABLE log_recherche CHANGE id_individu id_individu INT DEFAULT NULL');
        $this->addSql('ALTER TABLE log_recherche ADD CONSTRAINT FK_A22587C8E3FC35B FOREIGN KEY (id_individu) REFERENCES individu (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_A22587C8E3FC35B ON log_recherche (id_individu)');
        $this->addSql('ALTER TABLE log_recherche CHANGE id_individu id_individu INT NOT NULL');
    }
}
