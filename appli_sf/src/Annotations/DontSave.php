<?php

namespace App\Annotations;

/**
 * À utiliser pour les attributs de la classe Settings qui ne doivent pas être sauvegardés en base
 *
 * @Annotation
 */
class DontSave
{

}
