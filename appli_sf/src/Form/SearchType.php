<?php

namespace App\Form;

use App\Data\SearchData;
use App\Form\Transformer\DateToStringTransformer;
use App\Helper\SearchHelper;
use App\Service\SettingsDataService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchType extends AbstractType
{

    private $transformer;

    /**
     * @var \App\Data\SettingsData
     */
    private $settingsData;

    public function __construct(DateToStringTransformer $transformer, SettingsDataService $settingsDataService)
    {
        $this->transformer = $transformer;
        $this->settingsData = $settingsDataService->getData();
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'agreeTerms',
                CheckboxType::class,
                [
                    'label'    => 'search.agreeTerms',
                    'mapped'   => false,
                    'required' => true,
                ]
            )
            ->add(
                'deathDate',
                TextType::class,
                [
                    'label'    => 'search.deathDate',
                    'required' => true,
                    'help'     => 'fieldHelp.numbersOnly',
                    'attr'     => [
                        'placeholder' => 'search.dateFormat',
                        'class'       => 'input-date',
                    ],
                ]
            )->add(
                'deathLocation',
                TextType::class,
                [
                    'label'    => 'search.deathLocation',
                    'required' => true,
                    'help'     => 'fieldHelp.town',
                ]
            )->add(
                'deathCertificateDate',
                TextType::class,
                [
                    'label'    => 'search.deathCertificateDate',
                    'required' => true,
                    'help'     => 'fieldHelp.numbersOnly',
                    'attr'     => [
                        'placeholder' => 'search.dateFormat',
                        'class'       => 'input-date',
                    ],
                ]
            )->add(
                'recipient',
                TextType::class,
                [
                    'label'    => 'search.recipient',
                    'required' => false,
                    'help'     => 'fieldHelp.recipient',
                ]
            )->add(
                'firstName',
                TextType::class,
                [
                    'label'    => 'search.firstName',
                    'required' => true,
                ]
            )->add(
                'middleName',
                TextType::class,
                [
                    'label'    => 'search.middleName',
                    'required' => false,
                ]
            )->add(
                'thirdName',
                TextType::class,
                [
                    'label'    => 'search.thirdName',
                    'required' => false,
                ]
            )->add(
                'useName',
                TextType::class,
                [
                    'label'    => 'search.useName',
                    'required' => $this->settingsData->getSearchByName() === SearchHelper::SEARCH_BY_USE_NAME,
                    'help'     => 'fieldHelp.useName',
                ]
            )->add(
                'civilName',
                TextType::class,
                [
                    'label'    => 'search.civilName',
                    'required' => $this->settingsData->getSearchByName() === SearchHelper::SEARCH_BY_CIVIL_NAME,
                ]
            )->add(
                'birthDate',
                TextType::class,
                [
                    'label'    => 'search.birthDate',
                    'required' => true,
                    'help'     => 'fieldHelp.numbersOnly',
                    'attr'     => [
                        'placeholder' => 'search.dateFormat',
                        'class'       => 'input-date',
                    ],
                ]
            )->add(
                'send',
                SubmitType::class,
                [
                    'label' => 'search.send',
                ]
            );

        $builder->get('deathDate')->addModelTransformer($this->transformer);
        $builder->get('deathCertificateDate')->addModelTransformer($this->transformer);
        $builder->get('birthDate')->addModelTransformer($this->transformer);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => SearchData::class,
            ]
        );
    }
}
