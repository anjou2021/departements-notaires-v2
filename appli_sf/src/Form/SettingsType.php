<?php

namespace App\Form;

use App\Data\SettingsData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Contracts\Translation\TranslatorInterface;

class SettingsType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array                                        $options
     *
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $imageConstraint = [
            new File(
                [
                    'maxSize'          => '2M',
                    'mimeTypes'        => [
                        'image/png',
                        'image/jpeg',
                        'image/gif',
                        'image/svg+xml',
                        'image/svg',
                    ],
                    'mimeTypesMessage' => $this->translator->trans('settings.error.imageFile'),
                ]
            ),
        ];

        $pdfImageConstraint = [
            new File(
                [
                    'maxSize'          => '1M',
                    'mimeTypes'        => [
                        'image/png',
                        'image/jpeg',
                        'image/gif',
                    ],
                    'mimeTypesMessage' => $this->translator->trans('settings.error.pdfImageFile'),
                ]
            ),
        ];

        $builder
            ->add(
                'appName',
                TextType::class,
                [
                    'required' => true,
                    'label'    => 'settings.appName',
                ]
            )
            ->add(
                'appUrl',
                TextType::class,
                [
                    'required'   => false,
                    'label'      => 'settings.appUrl',
                    'empty_data' => '',
                ]
            )
            ->add(
                'appLogo',
                FileType::class,
                [
                    'required'    => false,
                    'label'       => 'settings.appLogo',
                    'constraints' => $imageConstraint,
                ]
            )
            ->add(
                'appFavicon',
                FileType::class,
                [
                    'required'    => false,
                    'label'       => 'settings.appFavicon',
                    'constraints' => $imageConstraint,
                ]
            )
            ->add(
                'departmentName',
                TextType::class,
                [
                    'required'   => false,
                    'label'      => 'settings.departmentName',
                    'empty_data' => '',
                ]
            )
            ->add(
                'departmentSite',
                TextType::class,
                [
                    'required'   => false,
                    'label'      => 'settings.departmentSite',
                    'empty_data' => '',
                ]
            )
            ->add(
                'departmentCilSite',
                TextType::class,
                [
                    'required'   => false,
                    'label'      => 'settings.departmentCilSite',
                    'empty_data' => '',
                ]
            )
            ->add(
                'soundexSearch',
                CheckboxType::class,
                [
                    'required' => false,
                    'label'    => 'settings.soundexSearch',
                ]
            )
            ->add(
                'connectionAttempts',
                IntegerType::class,
                [
                    'required'   => false,
                    'label'      => 'settings.connectionAttempts',
                    'attr'       => ['min' => 0, 'max' => 10],
                    'empty_data' => 3,
                ]
            )
            ->add(
                'searchByName',
                ChoiceType::class,
                [
                    'choices'  => [
                        'settings.searchByName.1' => 1,
                        'settings.searchByName.2' => 2,
                    ],
                    'multiple' => false,
                    'expanded' => true,
                    'required' => true,
                    'label'    => 'settings.searchByName.label',
                ]
            )
            ->add(
                'deathAlert',
                CheckboxType::class,
                [
                    'required' => false,
                    'label'    => 'settings.deathAlert'
                ]
            )->add(
                'deathAlertMail',
                TextType::class,
                [
                    'required' => false,
                    'label'    => 'settings.deathAlertMail',
                ]
            )
            ->add(
                'mailSignature',
                TextareaType::class,
                [
                    'required'   => false,
                    'label'      => 'settings.mailSignature',
                    'empty_data' => '',
                ]
            )
            ->add(
                'mailIntroduction',
                TextareaType::class,
                [
                    'required'   => false,
                    'label'      => 'settings.mailIntroduction',
                    'empty_data' => '',
                ]
            )
            ->add(
                'mailNotaBene',
                TextareaType::class,
                [
                    'required'   => false,
                    'label'      => 'settings.mailNotaBene',
                    'empty_data' => '',
                ]
            )
            ->add(
                'pdfDestinationCity',
                TextType::class,
                [
                    'required'   => false,
                    'label'      => 'settings.pdfDestinationCity',
                    'empty_data' => '',
                ]
            )
            ->add(
                'pdfLogo1',
                FileType::class,
                [
                    'required'    => false,
                    'label'       => 'settings.pdfLogo1',
                    'constraints' => $pdfImageConstraint,
                ]
            )
            ->add(
                'pdfLogo2',
                FileType::class,
                [
                    'required'    => false,
                    'label'       => 'settings.pdfLogo2',
                    'constraints' => $pdfImageConstraint,
                ]
            )
            ->add(
                'pdfServiceLogo',
                FileType::class,
                [
                    'required'    => false,
                    'label'       => 'settings.pdfServiceLogo',
                    'constraints' => $pdfImageConstraint,
                ]
            )
            ->add(
                'pdfSignature',
                FileType::class,
                [
                    'required'    => false,
                    'label'       => 'settings.pdfSignature',
                    'constraints' => $pdfImageConstraint,
                    'empty_data'  => '',
                ]
            )
            ->add(
                'pdfSuccessionMail',
                TextType::class,
                [
                    'required'   => false,
                    'label'      => 'settings.pdfSuccessionMail',
                    'empty_data' => '',
                ]
            )
            ->add(
                'pdfDepartmentalHouseName',
                TextType::class,
                [
                    'required'   => false,
                    'label'      => 'settings.pdfDepartmentalHouseName',
                    'empty_data' => '',
                ]
            )
            ->add(
                'pdfDepartmentalHousePhone',
                TextType::class,
                [
                    'required'   => false,
                    'label'      => 'settings.pdfDepartmentalHousePhone',
                    'empty_data' => '',
                ]
            )
            ->add(
                'pdfCSS',
                FileType::class,
                [
                    'required'    => false,
                    'label'       => 'settings.pdfCSS',
                    'constraints' => [
                        new File(
                            [
                                'maxSize'          => '100k',
                                'mimeTypes'        => [
                                    'text/css',
                                    'text/plain',
                                ],
                                'mimeTypesMessage' => $this->translator->trans('settings.error.cssFile'),
                            ]
                        ),
                    ],
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => SettingsData::class,
            ]
        );
    }
}
