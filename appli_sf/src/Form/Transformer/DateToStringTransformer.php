<?php

namespace App\Form\Transformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Contracts\Translation\TranslatorInterface;

class DateToStringTransformer implements DataTransformerInterface
{
    protected $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function transform($date)
    {
        if ($date) {
            return $date->format('d/m/Y');
        }

        return '';
    }

    public function reverseTransform($dateString)
    {
        $date = \DateTime::createFromFormat('d/m/Y', $dateString);
        $date->setTime(0, 0, 0);

        if (!$date) {
            $failure = new TransformationFailedException();
            $failure->setInvalidMessage(
                $this->translator->trans('formError.date'),
                [
                    '{{ value }}' => $dateString,
                ]
            );

            throw $failure;
        }

        return $date;
    }
}
