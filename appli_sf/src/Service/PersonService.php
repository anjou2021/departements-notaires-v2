<?php

namespace App\Service;

use App\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;

class PersonService
{
    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }

    /**
     * Récupère un individu à partir de son num_ind
     *
     * @param int $personNum
     *
     * @return Person
     */
    public function getPerson(int $personNum): Person
    {
        /** @var \App\Entity\Person|null $person */
        $person = $this->entityManager->getRepository(Person::class)->findOneBy(['personNum' => $personNum]);

        return $person;
    }
}
