<?php

namespace App\Service;

use App\Data\SettingsData;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class SettingService.
 */
class SettingsDataService
{

    /**
     * @var SettingsData
     */
    private $settingsData;

    /**
     * SettingDataService constructor.
     *
     * @param SettingsData           $settingsData
     * @param EntityManagerInterface $entityManager
     *
     * @throws \ReflectionException
     */
    public function __construct(SettingsData $settingsData, EntityManagerInterface $entityManager)
    {
        $this->settingsData = $settingsData->initFromDatabase($entityManager);
    }

    /**
     * Récupère les paramètres du site
     *
     * @return SettingsData
     */
    public function getData(): SettingsData
    {
        return $this->settingsData;
    }
}
