<?php

namespace App\Service;

use App\Entity\Settings;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class SettingService.
 */
class SettingService
{
    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     *
     * @param string $name
     *
     * @return string
     */
    public function getValue(string $name)
    {
        /** @var \App\Entity\Settings|null $setting */
        $setting = $this->em->getRepository(Settings::class)->findOneBy(['name' => $name]);

        if ($setting !== null) {
            return $setting->getValue();
        }

        return '';
    }
}
