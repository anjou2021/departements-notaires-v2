<?php

namespace App\Controller;

use App\Form\ChangePasswordType;
use App\Form\Model\ChangePassword;
use App\Helper\MessageHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class AccountController extends BaseController
{
    /**
     * Affiche les infos sur le compte courant
     *
     * @Route("/account/infos", name="account_infos")
     */
    public function infosAction()
    {
        $user = $this->getUser();

        return $this->render(
            'account/infos.html.twig',
            ['user' => $user]
        );
    }

    /**
     * Change le mot de passe de l'utilisateur courant
     *
     * @Route("/account/change-password", name="account_change_password")
     *
     * @param Request                      $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param TranslatorInterface          $translator
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function changePasswordAction(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        TranslatorInterface $translator
    ) {
        $user = $this->getUser();

        $changePassword = new ChangePassword($translator);
        $form = $this->createForm(ChangePasswordType::class, $changePassword);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $newPlainPassword = $form->get('password')['first']->getData();

            $newEncodedPassword = $passwordEncoder->encodePassword($user, $newPlainPassword);
            $user->setPassword($newEncodedPassword);

            $entityManager->flush();

            $this->addFlash(MessageHelper::MESSAGE_SUCCESS, $translator->trans('changePassword.success'));

            return $this->redirectToRoute('account_infos');
        }

        return $this->render(
            'account/change_password.html.twig',
            [
                'changePasswordForm' => $form->createView(),
            ]
        );
    }
}
