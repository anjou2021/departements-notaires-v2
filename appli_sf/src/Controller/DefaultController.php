<?php

namespace App\Controller;

use App\Helper\UserHelper;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class DefaultController extends BaseController
{

    /**
     * Contenu par défaut du site (redirection sur la page de login si non connecté)
     *
     * @Route("/", name="index")
     *
     * @param Security $security
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Security $security)
    {
        if ($security->isGranted(UserHelper::ROLE_ADMIN)) {
            return $this->redirect('admin');
        }

        return $this->redirect('search');
    }

    /**
     * Affiche l'aide
     *
     * @Route("/help", name="help")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function helpAction()
    {
        return $this->render('help.html.twig');
    }

    /**
     * Affiche les CGU
     *
     * @Route("/terms_of_use", name="terms_of_use")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function termsOfUseAction()
    {
        return $this->render('terms_of_use.html.twig');
    }
}
