<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Helper\MessageHelper;
use App\Service\MailService;
use App\Service\SettingsDataService;
use Hackzilla\PasswordGenerator\Generator\RequirementPasswordGenerator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mime\Exception\RfcComplianceException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin")
 */
class UserController extends BaseController
{
    /**
     * @var int
     */
    protected $passwordLength;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * UserController constructor.
     *
     * @param SettingsDataService $settingsDataService
     * @param int                 $passwordLength
     * @param TranslatorInterface $translator
     * @param string              $appTheme
     */
    public function __construct(
        SettingsDataService $settingsDataService,
        int $passwordLength,
        TranslatorInterface $translator,
        string $appTheme
    ) {
        parent::__construct($settingsDataService, $appTheme);

        $this->passwordLength = $passwordLength ?: 12;
        $this->translator = $translator;
    }

    /**
     * Liste les utilisateurs
     *
     * @Route("/user/list", name="admin_user_list")
     */
    public function listAction()
    {
        $userRepository = $this->getDoctrine()->getRepository(User::class);

        $users = $userRepository->findAll();

        return $this->render(
            'admin/user/list.html.twig',
            [
                'users' => $users,
            ]
        );
    }

    /**
     * Affiche le formulaire d'ajout d'un compte
     *
     * @Route("/user/add", name="admin_user_add")
     *
     * @param Request                      $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MailService                  $mailService
     * @param TokenGeneratorInterface      $tokenGenerator
     *
     * @return Response
     * @throws \Hackzilla\PasswordGenerator\Exception\CharactersNotFoundException
     * @throws \Hackzilla\PasswordGenerator\Exception\ImpossibleMinMaxLimitsException
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function addAction(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        MailService $mailService,
        TokenGeneratorInterface $tokenGenerator
    ) {
        $user = new User();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        // Si sélection d'un autre rôle on recharge juste le formulaire sans le valider
        $updateForm = (bool)$request->request->get('updateForm');

        if (!$updateForm && $form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            // Génération du mot de passe
            $generator = new RequirementPasswordGenerator();

            $generator
                ->setLength($this->passwordLength)
                ->setUppercase(true)
                ->setLowercase(true)
                ->setNumbers(true)
                ->setSymbols(true)
                ->setMinimumCount(RequirementPasswordGenerator::PARAMETER_SYMBOLS, 1)
                ->setMaximumCount(RequirementPasswordGenerator::PARAMETER_SYMBOLS, 3)
                ->setAvoidSimilar(true);


            $plainPassword = $generator->generatePassword();
            $password = $passwordEncoder->encodePassword($user, $plainPassword);
            $user->setPassword($password);

            // Création du token pour l'envoi de mail
            $token = $tokenGenerator->generateToken();

            $user->setTokenReset($token);

            $entityManager->persist($user);
            $entityManager->flush();

            $subject = $this->translator->trans(
                'addUser.confirmEmail.subject',
                ['%appName%' => $this->settingsData->getAppName()]
            );

            $this->sendResetPasswordMail($user, $mailService, $subject);

            return $this->redirectToRoute('admin_user_list');
        }

        return $this->render(
            'admin/user/add.html.twig',
            [
                'userForm' => $form->createView(),
            ]
        );
    }

    /**
     * Affiche le formulaire d'édition d'un compte
     *
     * @Route("/user/{id}/edit", name="admin_user_edit")
     *
     * @param User                         $user
     * @param Request                      $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     *
     * @return Response
     */
    public function editAction(
        User $user,
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        $updateForm = (bool)$request->request->get('updateForm');

        if (!$updateForm && $form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash(MessageHelper::MESSAGE_SUCCESS, $this->translator->trans('userEdit.updated'));

            return $this->redirectToRoute(
                'admin_user_edit',
                [
                    'id' => $user->getId(),
                ]
            );
        }

        return $this->render(
            'admin/user/edit.html.twig',
            [
                'userForm' => $form->createView(),
            ]
        );
    }

    /**
     * Supprime un compte à partir de son id
     *
     * @Route("/user/{id}/remove", name="admin_user_remove")
     *
     * @param Request $request
     * @param User    $user
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction(Request $request, User $user)
    {
        $submittedToken = $request->request->get('token');

        if ($this->isCsrfTokenValid('delete-user', $submittedToken)) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();

            $this->addFlash(MessageHelper::MESSAGE_SUCCESS, $this->translator->trans('userDelete.success'));
        } else {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $this->translator->trans('userDelete.error'));
        }

        return $this->redirectToRoute('admin_user_list');
    }

    /**
     * Désactive un compte à partir de son id
     *
     * @Route("/user/{id}/disable", name="admin_user_disable")
     *
     * @param Request $request
     * @param User    $user
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function disableAction(Request $request, User $user)
    {
        $submittedToken = $request->request->get('token');

        if ($this->isCsrfTokenValid('disable-user', $submittedToken)) {
            $entityManager = $this->getDoctrine()->getManager();
            $user->setDisabled(true);
            $entityManager->flush();

            $this->addFlash(MessageHelper::MESSAGE_SUCCESS, $this->translator->trans('userDisable.success'));
        } else {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $this->translator->trans('userDisable.error'));
        }

        return $this->redirectToRoute('admin_user_list');
    }

    /**
     * Active un compte à partir de son id
     *
     * @Route("/user/{id}/enable", name="admin_user_enable")
     *
     * @param Request $request
     * @param User    $user
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function enableAction(Request $request, User $user)
    {
        $submittedToken = $request->request->get('token');

        if ($this->isCsrfTokenValid('enable-user', $submittedToken)) {
            $entityManager = $this->getDoctrine()->getManager();
            $user->setDisabled(false);
            $entityManager->flush();

            $this->addFlash(MessageHelper::MESSAGE_SUCCESS, $this->translator->trans('userEnable.success'));
        } else {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $this->translator->trans('userEnable.error'));
        }

        return $this->redirectToRoute('admin_user_list');
    }

    /**
     * Réinitialisation du mot de passe d'un compte à partir de son id
     *
     * @Route("/user/{id}/reset_password", name="admin_user_reset_password")
     *
     * @param Request                 $request
     * @param User                    $user
     * @param MailService             $mailService
     * @param TokenGeneratorInterface $tokenGenerator
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function resetPasswordAction(
        Request $request,
        User $user,
        MailService $mailService,
        TokenGeneratorInterface $tokenGenerator
    ) {
        $submittedToken = $request->request->get('token');

        if ($this->isCsrfTokenValid('resetPassword-user', $submittedToken)) {
            $entityManager = $this->getDoctrine()->getManager();

            // Création du token pour l'envoi de mail
            $token = $tokenGenerator->generateToken();

            $user->setTokenReset($token);
            $entityManager->flush();

            $subject = $this->translator->trans(
                'adminResetPassword.mailSubject',
                ['%appName%' => $this->settingsData->getAppName()]
            );

            $this->sendResetPasswordMail($user, $mailService, $subject);

            $this->addFlash(MessageHelper::MESSAGE_SUCCESS, $this->translator->trans('userResetPassword.success'));
        } else {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $this->translator->trans('userResetPassword.error'));
        }

        return $this->redirectToRoute('admin_user_list');
    }

    /**
     * Envoie du mail de réinitialisation du mot de passe
     *
     * @param User        $user
     * @param MailService $mailService
     * @param string      $subject
     *
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    private function sendResetPasswordMail(User $user, MailService $mailService, string $subject)
    {
        $url = $this->generateUrl(
            'init_password',
            ['token' => $user->getTokenReset()],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        $mailRecipients = [$user->getEmail()];
        $mailHtml = $this->renderView(
            'emails/add_user.html.twig',
            [
                'name'    => $user->getName(),
                'initUrl' => $url,
            ]
        );

        try {
            $mailService->sendHtmlMail($mailRecipients, $subject, $mailHtml);
        } catch (TransportExceptionInterface $exception) {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $exception->getMessage());
        } catch (RfcComplianceException $exception) {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $exception->getMessage());
        }
    }
}
