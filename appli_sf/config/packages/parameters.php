<?php

$container->setParameter(
    'liip_imagine.data_root',
    dirname(realpath(__DIR__.'/../../public/uploads')) ?: '%kernel.project_dir%/public'
);
