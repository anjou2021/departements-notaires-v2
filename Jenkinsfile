#!/usr/bin/env groovy

pipeline {
  options {
    buildDiscarder(logRotator(numToKeepStr: '10'))
  }
  agent {
    label 'docker&&ovh'
  }
  environment {
     PROJECT = sh(script: './jenkins-env.sh PROJECT', returnStdout: true).trim()
     ZULIP_STREAM = sh(script: './jenkins-env.sh ZULIP_STREAM', returnStdout: true).trim()
     NEXUS_REPO = sh(script: './jenkins-env.sh NEXUS_REPO', returnStdout: true).trim()
     TYPE = sh(script: './jenkins-env.sh TYPE', returnStdout: true).trim()
     VERSION = sh(script: './jenkins-env.sh VERSION', returnStdout: true).trim()
     VERSION_IS_FOR_TESTING = sh(script: './jenkins-env.sh VERSION_IS_FOR_TESTING', returnStdout: true).trim()
     BASE_URL_UPLOAD = sh(script: './jenkins-env.sh BASE_URL_UPLOAD', returnStdout: true).trim()
     APP_FILENAME_TAR = sh(script: './jenkins-env.sh APP_FILENAME_TAR', returnStdout: true).trim()
     LATEST_URL = sh(script: './jenkins-env.sh LATEST_URL', returnStdout: true).trim()
     LATEST_APP_FILENAME = sh(script: './jenkins-env.sh LATEST_APP_FILENAME', returnStdout: true).trim()
     PHP_VERSION = sh(script: './jenkins-env.sh PHP_VERSION', returnStdout: true).trim()
     COMPOSER_VERSION = sh(script: './jenkins-env.sh COMPOSER_VERSION', returnStdout: true).trim()
     NODE_VERSION = sh(script: './jenkins-env.sh NODE_VERSION', returnStdout: true).trim()
     ENVIRONMENT = 'jenkins-release'
  }
  stages {
    stage('Notify') {
      steps {
        script {
          zulipSend stream: "$ZULIP_STREAM", topic: "jenkins-$TYPE", message: ":gear: Start build $PROJECT : $VERSION"
        }
      }
    }
    stage('Build PHP') {
      steps {
        script {
          def phpImage = docker.build(
            "php-image",
            "--build-arg PHP_VERSION=$PHP_VERSION --build-arg COMPOSER_VERSION=$COMPOSER_VERSION --build-arg ENVIRONMENT=$ENVIRONMENT ./docker/php"
          )

          phpImage.inside('-v "/var/lib/jenkins/composer/auth.json:/home/.composer/auth.json"') {
            dir('appli_sf') {
              stage("Composer install") {
                sh 'composer install --no-progress --no-dev --no-suggest --no-interaction --no-scripts --optimize-autoloader'
              }
            }
          }
        }
      }
    }
    stage('Build Node') {
      steps {
        script {
          def nodeImage = docker.build(
            "node-image",
            "--build-arg NODE_VERSION=$NODE_VERSION ./docker/node"
          )

          nodeImage.inside() {
            sh 'cd appli_sf && yarn && yarn run build'
          }
        }
      }
    }
    stage('Prepare files') {
      steps {
        script {
          sh 'ls final > /dev/null 2>&1 && rm -fr final || true'
          sh 'mkdir final && cp -RLp appli_sf final/app || true'
          sh 'echo "$VERSION" > final/build-version'
          sh 'rm -rf final/app/node_modules || true'
          sh 'chmod u+x final/app/bin/*'
        }
      }
    }
    stage('Package') {
      steps {
        script {
          sh 'cd final && tar -chpzf ../$APP_FILENAME_TAR *'
        }
      }
    }
    stage('Upload') {
      failFast true
      parallel{
        stage('App tar upload') {
          steps {
            withCredentials([usernameColonPassword(credentialsId: 'nexus3-jenkins', variable: 'NEXUS3_AUTH')]) {
              sh 'curl -v --user "$NEXUS3_AUTH" --upload-file ./$APP_FILENAME_TAR $BASE_URL_UPLOAD/tar/$APP_FILENAME_TAR'
            }
          }
        }
      }
    }
    stage('Push version in latest file') {
      when {
        environment name: 'VERSION_IS_FOR_TESTING', value: '0';
      }
      steps {
        withCredentials([usernameColonPassword(credentialsId: 'nexus3-jenkins', variable: 'NEXUS3_AUTH')]) {
          sh 'echo "${VERSION}" > version-latest.txt'
          sh 'curl -v --user "$NEXUS3_AUTH" --upload-file ./version-latest.txt $LATEST_URL/$LATEST_APP_FILENAME'
        }
      }
    }
  }
  post {
    always {
      deleteDir()
      zulipNotification smartNotification: "${TYPE=='release'?'disabled':'enabled'}", stream: "${ZULIP_STREAM}", topic: "jenkins-${TYPE}"
    }
  }
}
