#!/bin/sh

version=$1

usage() {
    echo "Déployer une version de l'appli."
    echo "Usage: $0 <version>"
    exit
}

if [ "$#" -ne 1 ]; then
    usage
fi

echo "Version à déployer : $version"

file="app-$version.tgz"

if [ -f "$file" ]; then
    echo "$file trouvé."
else
    echo "$file non trouvé."
    exit 1
fi

# Efface les fichiers (sauf les médias et ceux pour la personnalisation)
echo "Suppression des anciens fichiers ..."
rm -rf app/assets
rm -rf app/config
rm -rf app/public/build
rm -rf app/public/bundles
rm -rf app/src
rm -rf app/templates/default
rm -rf app/tests
rm -rf app/translations/messages.fr.yaml
rm -rf app/vendor

echo "Décompression ..."
sh -c "tar xzf $file"

echo "Efface les caches ..."
sh -c "./bin/console cache:clear"

echo "Mise à jour BDD (si nécessaire) ..."
sh -c "./bin/console doctrine:migrations:migrate --no-interaction"
